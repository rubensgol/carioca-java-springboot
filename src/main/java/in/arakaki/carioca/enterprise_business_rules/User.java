package in.arakaki.carioca.enterprise_business_rules;

import java.util.UUID;

public class User {
    private String name;
    private UUID id;

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
