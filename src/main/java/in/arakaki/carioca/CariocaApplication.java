package in.arakaki.carioca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"in.arakaki.carioca"})

public class CariocaApplication{
	public static void main(String[] args){
		SpringApplication.run(CariocaApplication.class, args);
	}
}

