package in.arakaki.carioca.framework_drivers;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import in.arakaki.carioca.enterprise_business_rules.User;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import java.util.HashMap;
import java.util.Map;

@Repository
public class Database implements IDatabase {

    public Database(NamedParameterJdbcTemplate template) {
        this.template = template;
    }
    NamedParameterJdbcTemplate template;

    @Override
    public List<User> findAll() {
        return template.query("select * from tbl_user", new UserRowMapper());
    }

    @Override
    public void insert(User user) {
        final String sql = "insert into tbl_user(name) values(:name)";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource()
                .addValue("name", user.getName());
        template.update(sql,param, holder);
    }

    @Override
    public void updateUser(User user) {
        final String sql = "update tbl_user set name=:name where id=:id";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", user.getName())
                .addValue("name", user.getId());
        template.update(sql,param, holder);

    }

    @Override
    public void deleteUser(User user) {
        final String sql = "delete from tbl_user where 'id'=:id";
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("id", user.getId());
        template.execute(sql,map,new PreparedStatementCallback<Object>() {
            @Override
            public Object doInPreparedStatement(PreparedStatement ps)
                    throws SQLException, DataAccessException {
                return ps.executeUpdate();
            }
        });
    }
}
