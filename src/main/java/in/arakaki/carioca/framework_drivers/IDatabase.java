package in.arakaki.carioca.framework_drivers;

import in.arakaki.carioca.enterprise_business_rules.User;
import java.util.List;

public interface IDatabase {
    List<User> findAll();

    void insert(User user);

    void updateUser(User user);

    void deleteUser(User user);

}
