package in.arakaki.carioca.framework_drivers;

import java.sql.ResultSet;
import java.sql.SQLException;
import in.arakaki.carioca.enterprise_business_rules.User;
import org.springframework.jdbc.core.RowMapper;
import java.util.UUID;

public class UserRowMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet rs, int arg1) throws SQLException {
        User emp = new User();
        emp.setId(UUID.fromString(rs.getString("uuid")));
        emp.setName(rs.getString("name"));
        return emp;
    }
}
