package in.arakaki.carioca.framework_drivers;

import com.google.api.gax.rpc.ApiException;
import com.google.cloud.pubsub.v1.TopicAdminClient;
import com.google.pubsub.v1.ProjectTopicName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.pubsub.integration.outbound.PubSubMessageHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.MessageHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class PubSubPublisher {

    private String projectId = "arakaki-in-company";
    private String topicId = "topic1" ;
    private String subscriptionId = "carioca";
    ProjectTopicName topic = ProjectTopicName.of(projectId, topicId);
    private static final Log LOGGER = LogFactory.getLog(PubSubPublisher.class);

    @MessagingGateway(defaultRequestChannel = "pubsubOutputChannel")
    public interface PubSubPublisherService {
        public void sendToPubsub(String text);
    }

    @Bean
    @ServiceActivator(inputChannel = "pubsubOutputChannel")
    public MessageHandler messageSender(PubSubTemplate pubsubTemplate) throws IOException {
        ProjectTopicName topic = ProjectTopicName.of(projectId, topicId);
        try (TopicAdminClient topicAdminClient = TopicAdminClient.create()) {
            topicAdminClient.createTopic(topic);
            System.out.printf("Topic %s:%s created.\n", topic.getProject(), topic.getTopic());
        } catch (ApiException e) {
            System.out.print(e.getStatusCode().getCode());
            System.out.print(e.isRetryable());
        }
        return new PubSubMessageHandler(pubsubTemplate, topicId);
    }

}
