package in.arakaki.carioca.interface_adapters;

import in.arakaki.carioca.enterprise_business_rules.User;
import in.arakaki.carioca.framework_drivers.IDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserService implements IUserService {

    @Autowired
    IDatabase database;

    @Override
    public List<User> findAll() {
        return database.findAll();
    }

    @Override
    public void insertUser(User user) {
        database.insert(user);
    }

    @Override
    public void updateUser(User user) {
        database.updateUser(user);
    }

    @Override
    public void deleteUser(User user) {
        database.deleteUser(user);
    }
}
