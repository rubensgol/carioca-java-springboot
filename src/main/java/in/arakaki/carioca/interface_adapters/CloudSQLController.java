package in.arakaki.carioca.interface_adapters;

import in.arakaki.carioca.enterprise_business_rules.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public class CloudSQLController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/user")
    public List<User> getUser() {
        return userService.findAll();
    }

    @PostMapping(value = "/user")
    public void inserUser(@RequestBody User user) {
        userService.insertUser(user);
    }

    @DeleteMapping(value = "/user")
    public void deletUser(@RequestBody User user){
        userService.deleteUser(user);
    }

    @PutMapping(value = "/user")
    public void update(@RequestBody User user){
        userService.updateUser(user);
    }
}
