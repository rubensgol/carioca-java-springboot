package in.arakaki.carioca.interface_adapters;

import in.arakaki.carioca.enterprise_business_rules.User;
import java.util.List;

public interface IUserService {
    List<User> findAll();

    void insertUser(User user);

    void updateUser(User user);

    void deleteUser(User user);
}
